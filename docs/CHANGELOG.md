# Changelog


## (unreleased)

### Changes

* 财务数据调整为异步下载方式, 性能提升十几倍. [bopo]

* 调整logger, 使用异步方式选择最优服务器ip. [bopo]

* 更新文档. [bopo]

### Other

* Unipath to pathlib. [bopo]

* Unipath to pathlib. [bopo]

* 格式化代码. [bopo]

* 格式化代码. [bopo]

* 项目整理. [bopo]

* Merge branch 'master' into develop. [bopo]

* !3 update mootdx/contrib/compat.py. Merge pull request !3 from b0p0m0f0/N/A. [bopo]

* Update mootdx/contrib/compat.py. 支持科创版指数基金。 [b0p0m0f0]


## v0.7.16 (2021-09-08)

### Changes

* 更新文档. [bopo]


## v0.7.15 (2021-09-08)

### Changes

* 更新文档. [bopo]

* 更新文档. [bopo]

* 修复分钟线数据读取bug. [bopo]

### Other

* Merge pull request #3 from mootdx/develop. [bopo.wang]

  chg: 修复分钟线数据读取bug

* Formant code. [bopo]

* Fix .pre-commit-config.yaml. [bopo]


## v0.7.14 (2021-08-24)

### Fix

* Update test. [bopo]

* Update test. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

### Other

* Update commit. [bopo]

* Merge branch 'develop' [bopo]

* Merge branch 'develop' of gitee.com:ibopo/mootdx. [bopo]

* Merge branch 'develop' of gitee.com:ibopo/mootdx into develop. [bopo]

* Update hotfix. [ibopo]

* Update hotfix. [ibopo]

* Merge branch 'develop' [ibopo]


## v0.7.12 (2021-08-04)

### Other

* Update quotes server. [bopo]

* Merge branch 'develop' of gitee.com:ibopo/mootdx into develop. [bopo]

* Update commit. [ibopo]

* Update commit. [ibopo]

* Merge branch 'develop' of gitee.com:ibopo/mootdx into develop. [ibopo]

* Merge branch 'develop' [ibopo]

* Merge branch 'develop' of gitee.com:ibopo/mootdx into develop. [ibopo]

* Merge branch 'master' of gitee.com:ibopo/mootdx. [ibopo]

  Conflicts:
      mootdx/quotes.py

* Update commit. [bopo]

* Merge branch 'develop' of gitee.com:ibopo/mootdx into develop. [bopo]

* Update docs. [bopo]


## v0.7.11 (2021-07-13)

### Changes

* Change connecton. [bopo]

* Add log. [bopo]

* Mrage. [bopo]

* Mrage. [bopo]

* Update adjust. [bopo]

* Update to_data. [bopo]

* Update requirements. [bopo]

* Change readme. [bopo]

* Change requirements. [bopo]

* Remove trade server. [bopo]

* Remove trade server. [bopo]

### Fix

* Reader path bug. [bopo]

* 修改. [bopo]

* 修改win下config目录问题. [bopo]

### Other

* Merge branch 'develop' [ibopo]

* Merge branch 'develop' of gitee.com:ibopo/mootdx into develop. [ibopo]

* Merge branch 'master' of gitee.com:ibopo/mootdx. [ibopo]

  Conflicts:
      mootdx/quotes.py

* !2 bugfix: 查询分笔成交offset不能为market code * bugfix: 查询分笔成交offset不能为market code. [dhrhe]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Merge branch 'develop' of gitee.com:ibopo/mootdx into develop. [bopo]

* Update tests. [bopo]

* Merge branch 'develop' of gitee.com:ibopo/mootdx into develop. [bopo]

* Update docs. [bopo]

* Clean tests. [bopo]

* Update docs. [bopo]

* Update bestip. [bopo]

* Update import. [bopo]

* Update timeout. [bopo]

* Update bestip. [bopo]

* Merge branch 'develop' of gitee.com:ibopo/mootdx into develop. [bopo]

* Update logger. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update block new test. [bopo]

* Update block new test. [bopo]

* Merge branch 'develop' [bopo]

* Update block new test. [bopo]

* Merge branch 'develop' [bopo]

* Update test. [bopo]

* Update docs. [bopo]

* Update test. [bopo]

* Update test. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update adjust. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Merge branch 'develop' [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Merge branch 'develop' of gitee.com:ibopo/mootdx into develop. [bopo]

* Update commit. [bopo]

* Add: added utils tests. [bopo]

* !1 Bug fix: 修复文件路径问题 Merge pull request !1 from bopomofo/master. [bopo]

* Bug fix: replace unipath with pathlib. [Bo Zheng]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Merge branch 'feature/rm_trade_server' into develop. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]


## v0.7.10 (2021-07-05)

### Changes

* Change connecton. [bopo]

* Add log. [bopo]

* Mrage. [bopo]

* Mrage. [bopo]

* Update adjust. [bopo]

* Update to_data. [bopo]

* Update requirements. [bopo]

* Change readme. [bopo]

* Change requirements. [bopo]

* Remove trade server. [bopo]

* Remove trade server. [bopo]

### Fix

* Reader path bug. [bopo]

* 修改. [bopo]

* 修改win下config目录问题. [bopo]

* Update commit. [bopo]

### Other

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Merge branch 'develop' of gitee.com:ibopo/mootdx into develop. [bopo]

* Update tests. [bopo]

* Merge branch 'develop' of gitee.com:ibopo/mootdx into develop. [bopo]

* Update docs. [bopo]

* Clean tests. [bopo]

* Update docs. [bopo]

* Update bestip. [bopo]

* Update import. [bopo]

* Update timeout. [bopo]

* Update bestip. [bopo]

* Merge branch 'develop' of gitee.com:ibopo/mootdx into develop. [bopo]

* Update logger. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update block new test. [bopo]

* Update block new test. [bopo]

* Merge branch 'develop' [bopo]

* Update block new test. [bopo]

* Merge branch 'develop' [bopo]

* Update test. [bopo]

* Update docs. [bopo]

* Update test. [bopo]

* Update test. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update adjust. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Merge branch 'develop' [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Merge branch 'develop' of gitee.com:ibopo/mootdx into develop. [bopo]

* Update commit. [bopo]

* Add: added utils tests. [bopo]

* !1 Bug fix: 修复文件路径问题 Merge pull request !1 from bopomofo/master. [bopo]

* Bug fix: replace unipath with pathlib. [Bo Zheng]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update reader. [bopo]

* Update reader. [bopo]

* Update transactions. [bopo]

* Update affair. [bopo]

* Update commit. [bopo]

* Update ext market bug. [bopo]

* Update commit. [bopo]

* Merge branch 'feature/rm_trade_server' into develop. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]


## v0.7.8 (2021-06-29)

### Other

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]


## v0.7.5 (2021-06-26)

### Other

* Update commit. [bopo]

* Merge branch 'develop' of gitee.com:ibopo/mootdx into develop. [bopo]

* Update tests. [bopo]

* Merge branch 'develop' of gitee.com:ibopo/mootdx into develop. [bopo]

* Update docs. [bopo]

* Clean tests. [bopo]

* Update docs. [bopo]

* Update bestip. [bopo]

* Update import. [bopo]

* Update timeout. [bopo]

* Update commit. [bopo]


## v0.7.3 (2021-06-23)

### Other

* Update bestip. [bopo]

* Merge branch 'develop' of gitee.com:ibopo/mootdx into develop. [bopo]

* Update logger. [bopo]

* Update commit. [bopo]


## v0.7.2 (2021-06-22)

### Other

* Update block new test. [bopo]

* Update block new test. [bopo]

* Merge branch 'develop' [bopo]

* Update block new test. [bopo]

* Merge branch 'develop' [bopo]

* Update test. [bopo]

* Update docs. [bopo]

* Update test. [bopo]

* Update test. [bopo]

* Merge branch 'develop' [bopo]

* Update commit. [bopo]

* !1 Bug fix: 修复文件路径问题 Merge pull request !1 from bopomofo/master. [bopo]

* Bug fix: replace unipath with pathlib. [Bo Zheng]


## v0.7.1 (2021-06-21)

### Other

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update adjust. [bopo]


## v0.7.0 (2021-06-17)

### Changes

* Change connecton. [bopo]

### Fix

* Reader path bug. [bopo]

* 修改. [bopo]

### Other

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Merge branch 'develop' of gitee.com:ibopo/mootdx into develop. [bopo]

* Update commit. [bopo]

* Add: added utils tests. [bopo]


## v0.6.10 (2021-06-07)

### Fix

* 修改win下config目录问题. [bopo]


## v0.6.9 (2021-06-04)

### Other

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]


## v0.6.8 (2021-06-02)

### Other

* Update commit. [bopo]

* Update commit. [bopo]


## v0.6.7 (2021-06-02)

### Changes

* Add log. [bopo]

* Mrage. [bopo]

* Mrage. [bopo]

* Update adjust. [bopo]

* Update to_data. [bopo]

* Update requirements. [bopo]

* Change readme. [bopo]

* Change requirements. [bopo]

* Remove trade server. [bopo]

* Remove trade server. [bopo]

### Other

* Update commit. [bopo]

* Merge branch 'feature/rm_trade_server' into develop. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]


## v0.6.6 (2021-01-30)

### Other

* Update reader. [bopo]

* Update reader. [bopo]

* Update transactions. [bopo]

* Update affair. [bopo]

* Update commit. [bopo]


## v0.6.5 (2021-01-30)

### New

* Fixed console bug, add docs pages. [bopo]

### Changes

* Update tests. [bopo]

### Fix

* Update commit. [bopo]

* Fixed console bug, add docs pages. [bopo]

### Other

* Update ext market bug. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Merge branch 'master' of https://github.com/bopo/mootdx. [bopo]

* Merge pull request #54 from bopo/develop. [bopo.wang]

  Develop

* Merge pull request #49 from cztchoice/master. [bopo.wang]

  fix execute error in python3.8; pretty output

* Fix execute error in python3.8; pretty output. [cztchoice]

* Merge pull request #52 from bopo/pyup-update-pytest-5.4.2-to-6.0.1. [bopo.wang]

  Update pytest to 6.0.1

* Update pytest from 5.4.2 to 6.0.1. [pyup-bot]


## v0.6.4 (2020-08-06)

### Other

* Update commit. [bopo]


## v0.6.3 (2020-08-06)

### Other

* Update commit. [bopo]


## v0.6.2 (2020-08-06)

### Other

* Update commit. [bopo]

* Update commit. [bopo]


## v0.6.0 (2020-08-06)

### Other

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Merge branch 'master' of https://github.com/bopo/mootdx. [bopo]

* Merge pull request #39 from busyluo/master. [bopo.wang]

  fixed bugs.

* Fixed bugs. [Xavier]

* Update commit. [bopo]

* Merge branch 'master' of https://github.com/bopo/mootdx. [bopo]

* Update README.rst. [bopo.wang]

* Update README.rst. [bopo.wang]

* Merge pull request #38 from bopo/pyup-update-mkdocs-1.1-to-1.1.1. [bopo.wang]

  Update mkdocs to 1.1.1

* Update mkdocs from 1.1 to 1.1.1. [pyup-bot]

* Merge pull request #37 from bopo/pyup-update-pytest-5.4.1-to-5.4.2. [bopo.wang]

  Update pytest to 5.4.2

* Update pytest from 5.4.1 to 5.4.2. [pyup-bot]

* Merge branch 'develop' [bopo]

* Add super awesome feature. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]


## v0.3.22 (2020-06-11)

### Other

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]


## v0.3.21 (2020-05-15)

### Other

* Update commit. [bopo]


## v0.3.20 (2020-05-14)

### Other

* 修改转债数据无法显示问题. [bopo]

* 修改转债数据无法显示问题. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Merge branch 'hotfix/0.3.18' into develop. [bopo]


## 0.3.18 (2020-04-21)

### Other

* Merge branch 'hotfix/0.3.18' [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update docss. [bopo]


## v0.3.18 (2020-04-14)

### Other

* Revert version. [bopo]

* Merge branch 'develop' [bopo]

* Clear files. [bopo]

* Formant code. [bopo]


## v0.3.17 (2020-04-08)

### Other

* Merge branch 'master' of https://github.com/bopo/mootdx. [bopo]

* Merge pull request #31 from bopo/pyup-update-mkdocs-0.17.3-to-1.1. [bopo.wang]

  Update mkdocs to 1.1

* Update mkdocs from 0.17.3 to 1.1. [pyup-bot]

* Merge pull request #33 from bopo/pyup-update-pytest-5.3.5-to-5.4.1. [bopo.wang]

  Update pytest to 5.4.1

* Update pytest from 5.3.5 to 5.4.1. [pyup-bot]

* Merge branch 'develop' [bopo]

* Update readme file. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Fixed bugs. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Merge branch 'release/v0.3.17' into develop. [bopo]

* Merge branch 'release/v0.3.17' [bopo]

* Update commit. [bopo]

* Update commit. [bopo]


## v0.3.16 (2020-03-06)

### Other

* Merge branch 'release/v0.3.16' [bopo]

* Merge branch 'hotfix/20200306' into develop. [bopo]

* Update commit. [bopo]

* Merge branch 'hotfix/20200306' [bopo]

* Update hotfix. [bopo]

* Modifed stocks method. [bopo]

* Merge pull request #30 from bopo/develop. [bopo.wang]

  Merge branch 'develop' of https://github.com/bopo/mootdx into develop

* Merge branch 'develop' of https://github.com/bopo/mootdx into develop. [bopo]

* Merge branch 'master' of https://github.com/bopo/mootdx. [bopo]

* Merge pull request #29 from bopo/develop. [bopo.wang]

  Merge pull request #28 from bopo/master

* Merge pull request #28 from bopo/master. [bopo.wang]

  Master


## v0.3.15 (2020-02-19)

### Other

* Merge branch 'release/v0.3.15' [bopo]

* Merge branch 'release/0.3.15' into develop. [bopo]


## 0.3.15 (2020-02-19)

### Other

* Merge branch 'release/0.3.15' [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]


## 0.3.14 (2020-02-18)

### Other

* Merge branch 'release/0.3.14' [bopo]

* Update version. [bopo]

* Update version. [bopo]

* Merge branch 'develop' of https://github.com/bopo/mootdx into develop. [bopo]

* Merge pull request #26 from bopo/pyup-update-pytest-5.3.3-to-5.3.5. [bopo.wang]

  Update pytest to 5.3.5

* Update pytest from 5.3.3 to 5.3.5. [pyup-bot]

* Fixed affair 404 bug. [bopo]

* Merge pull request #24 from bopo/pyup-update-pytest-5.3.2-to-5.3.3. [bopo.wang]

  Update pytest to 5.3.3

* Update pytest from 5.3.2 to 5.3.3. [pyup-bot]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Merge branch 'develop' of https://github.com/bopo/mootdx into develop. [bopo]

* Merge pull request #22 from bopo/pyup-update-pytest-5.3.0-to-5.3.2. [bopo.wang]

  Update pytest to 5.3.2

* Update pytest from 5.3.0 to 5.3.2. [pyup-bot]

* Merge pull request #19 from bopo/pyup-pin-pytest-5.3.0. [bopo.wang]

  Pin pytest to latest version 5.3.0

* Pin pytest to latest version 5.3.0. [pyup-bot]

* Merge pull request #17 from bopo/master. [bopo.wang]

  Merge branch 'release/v0.3.12'

* Update commit. [bopo]


## v0.3.12 (2019-11-16)

### Other

* Merge branch 'release/v0.3.12' [bopo]

* Fixed console bug, add docs pages. [bopo]

* Fixed console bug, add docs pages. [bopo]

* Update readme. [bopo]

* Update readme. [bopo]

* Update readme. [bopo]


## v0.3.11 (2019-11-14)

### Other

* Merge branch 'release/v0.3.11' [bopo]

* Change version. [bopo]

* Fixed console. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Config. [bopo]

* Config. [bopo]

* Merge pull request #8 from bopo/pyup-pin-httmock-1.3.0. [bopo.wang]

  Pin httmock to latest version 1.3.0

* Pin httmock to latest version 1.3.0. [pyup-bot]

* Merge pull request #13 from bopo/pyup-pin-tqdm-4.37.0. [bopo.wang]

  Pin tqdm to latest version 4.37.0

* Pin tqdm to latest version 4.37.0. [pyup-bot]

* Merge branch 'release/v0.3.7' [bopo]

* Update config. [bopo]

* Update config. [bopo]

* Update config. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Merge branch 'release/v0.3.6' [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Merge pull request #11 from bopo/master. [bopo.wang]

  Master

* Merge branch 'release/v0.3.5' [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Merge branch 'develop' of https://github.com/bopo/mootdx into develop. [bopo]

* Merge pull request #9 from bopo/master. [bopo.wang]

  Master

* Update commit. [bopo]

* Update default bestip. [bopo]

* Merge branch 'release/v0.3.4' [bopo]

* Update readme. [bopo]

* Update readme. [bopo]

* Merge branch 'release/v0.3.3' [bopo]

* Update version. [bopo]

* Update ext quote symbol format. [bopo]

* Update affair. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Merge branch 'release/v0.3.2-fiexed' [bopo]

* Update commit. [bopo]

* Merge branch 'release/v0.3.2' [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Fixed tushare/stock/shibor.py. [bopo]

* Fixed tushare/stock/shibor.py. [bopo]

* Merge branch 'release/0.2.5' [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update README.rst. [bopo.wang]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Import optimizer. [bopo]

* Import optimizer. [bopo]

* Update commit. [bopo]

* Update commit. [bopo]

* Update commit. [Travis CI]

* Update commit. [Travis CI]

* Update commit. [Travis CI]

* Update commit. [Travis CI]

* Update commit. [Travis CI]

* Update commit. [Travis CI]

* Update commit. [Travis CI]

* Update commit. [Travis CI]

* Update commit. [Travis CI]

* Update commit. [Travis CI]

* 修改一些小bug. [Travis CI]

* 做了一些调整. [Travis CI]

* Add test data files. [Travis CI]

* New version 0.1.8. [Travis CI]

* New version 0.1.8. [Travis CI]

* Modify LiveBar to Quotes. [bopo]

* Update. [bopo]

* Add cli. [bopo]

* Update. [bopo]

* Update tests and find_path. [bopo]

* Up tests. [bopo]

* Up test. [bopo]

* Update ver. [bopo]

* Update. [bopo]

* Update. [bopo]

* Update. [bopo]

* Add download. [bopo]

* Add. [bopo]

* Add:w. [bopo]

* Aa. [bopo]

* Add. [bopo]

* A. [bopo]

* Rename. [bopo]

* Readem. [bopo]

* Initail. [bopo]

* Removed requirements_dev.txt. [bopo]

* Fixed tox env. [bopo]

* Aa. [bopo]

* Readme. [bopo]

* Aa. [bopo]

* Merge branch 'develop' of https://github.com/bopo/cardbin into develop dd. [bopo]

* Merge pull request #1 from bopo/master. [bopo.wang]

  Merge branch 'release/v0.1.2'

* Merge branch 'release/v0.1.2' [bopo]

  pub

* Aa. [bopo]

* Add. [bopo]

* Aa. [bopo]

* Add pki file. [bopo]

* Initial. [bopo]

* Initial commit. [bopo]
